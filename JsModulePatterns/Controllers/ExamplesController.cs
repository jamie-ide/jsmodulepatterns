﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JsModulePatterns.Controllers
{
    public class ExamplesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Example1()
        {
            return View();
        }

        public ActionResult Example2()
        {
            return View();
        }

        public ActionResult Example3()
        {
            return View();
        }

        public ActionResult Example4()
        {
            return View();
        }

        public JsonResult LowTemps()
        {
            var lowTemps = new List<StateTemperatureViewModel>
            {
                new StateTemperatureViewModel("Maine", new DateTime(2009, 1, 16), -50),
                new StateTemperatureViewModel("New Hampshire", new DateTime(1934, 1, 29), -47),
                new StateTemperatureViewModel("Vermont", new DateTime(1933, 12, 30), -50)
            };

            return Json(lowTemps, JsonRequestBehavior.AllowGet);
        }

        public JsonResult HighTemps()
        {
            var highTemps = new List<StateTemperatureViewModel>
            {
                new StateTemperatureViewModel("Maine", new DateTime(1911, 7, 10), 105),
                new StateTemperatureViewModel("New Hampshire", new DateTime(1911, 7, 4), 106),
                new StateTemperatureViewModel("Vermont", new DateTime(1911, 7, 4), 105)
            };

            return Json(highTemps, JsonRequestBehavior.AllowGet);
        }

        private class StateTemperatureViewModel
        {
            public StateTemperatureViewModel(string state, DateTime date, double temperature)
            {
                State = state;
                Date = date.ToShortDateString();
                Temperature = temperature;
            }

            public string State { get; private set; }
            public string Date { get; private set; }
            public double Temperature { get; private set; }
        }
    }
}